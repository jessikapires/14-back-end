import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import Vitrine from './Vitrine';
import Homepage from './Homepage';
import Cadastro from './cadastro';
import Login from './login';
import Pagina from './pagina';

const Navigation = () => {
    return (
        <BrowserRouter >
            <div>
                <Route exact path={"/home"} component={Homepage} />
                <Route exact path={"/vitrine"} component={Vitrine} />
                <Route exact path={"/Cadastro"} component={Cadastro}/>
                <Route exact path={"/login"} component={Login}/>
                <Route exact path={"/pagina"} component={Pagina}/>
               
            </div>
        </BrowserRouter>
    );
};

export default Navigation;