import React from 'react';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';



const CarouselHome = () => {
    return (

        <Carousel
            className="carousel"
            infiniteLoop
            useKeyboardArrows autoPlay
            showThumbs={false}
            showIndicators={false}
            showStatus={false}
        >
            <div>
                <img src= "https://www.azulviagens.com.br/handlers/ImageRequest.ashx?path=%2f%2fimages.azulviagens.com.br%2fimages%2fupload%2fBanner+Home%2fBanners_Semana_06_06_2019%2fBanner_VCP_EZE_.jpg&mediaSize=lg&imgSize=bigger" alt="img" />
            </div>
            <div>
                <img src= "https://www.azulviagens.com.br/handlers/ImageRequest.ashx?path=%2f%2fimages.azulviagens.com.br%2fimages%2fupload%2fBanner+Home%2fCaldas_Country_2019%2fBanner_Caldas_Country.jpg&mediaSize=lg&imgSize=bigger" alt="img" />
            </div>
            <div>
                <img src="https://www.azulviagens.com.br/handlers/ImageRequest.ashx?path=%2f%2fimages.azulviagens.com.br%2fimages%2fupload%2fBanner+Home%2fBanners_Semana_06_06_2019%2fBanner_Natal_Luz_Gramado.jpg&mediaSize=lg&imgSize=bigger" alt="img" />
            </div>
        </Carousel>
    )
}

export default CarouselHome;