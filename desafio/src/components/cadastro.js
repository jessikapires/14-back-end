import React from 'react'
import './cadastro.css'
import { NavLink } from 'react-router-dom'




const Cadastro = () => {
    return (

        <div className="container-cadastro1">
            <img className="imagemFundo" src="https://i.pinimg.com/originals/f4/87/d2/f487d2aeab7aebbc090c38aa7c8d05c1.jpg" alt="img" />
            <div className="container-cadastro">
                <p className="formularioCadastro">Prencha corretamente os campos abaixo:</p>
                <p className="nomeCadastro"> Nome completo:<input /></p>
                <p className="idade"> Idade:<input /></p>
                <p className="email"> E-mail:<input /></p>
                <p className="senhaCadastro"> Senha:<input /></p>
                <NavLink to={"/login"}><button className="botaocadastro"><span>Finalizar</span></button></NavLink>
                

            </div>
        </div>
    )

}


export default Cadastro;