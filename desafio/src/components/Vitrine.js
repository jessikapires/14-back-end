import React from 'react'
import './vitrine.css'


const Vitrine = () => {

    return (
        <div>
            <p className="Promocao">PROMOÇÃO AZUL VIAGENS</p>
            <div >

                <img className="pacoteTodos" src="https://media.staticontent.com/media/pictures/b4949985-55d5-4b4a-a041-cc28e3c2d020" alt="img" />
                <img className="pacoteTodos" src="http://www.viabrturismo.com.br/pacotesinternacionais/europa/imagens-slider/17603874_m-londres.jpg" alt="img" />
                <img className="pacoteTodos" src="https://www.delambre-paris-hotel.com/inc/uploads/sites/64/2017/08/croisiere-paris.jpg" alt="img" />
                <div className="pacote-container">
                    <p className="nomeTodos"> Orlando 7 dias </p>
                    <p className="valorTodos">  A partir de  </p>
                    <p className="valorTodos1">  R$ 9.500,00 por pessoa </p>
                </div>
                <div className="pacote-container">
                    <p className="nomeTodos">Londres em 7 dias</p>
                    <p className="valorTodos"> A partir de </p>
                    <p className="valorTodos1"> R$ 10.500,00 para 2 pessoas</p>
                </div>
                <div className="pacote-container">
                    <p className="nomeTodos">Paris em  dias</p>
                    <p className="valorTodos"> A partir de </p>
                    <p className="valorTodos1">R$ 5.200,00 por pessoa</p>
                </div>

                <img className="pacoteTodos" src="http://www.conexaoboasnoticias.com.br/wp-content/uploads/2017/12/torre-de-belem-lisboa.jpg" alt="img" />
                <img className="pacoteTodos" src="https://media.staticontent.com/media/pictures/861307e7-3447-48c3-96ff-f66b7745f39b" alt="img" />
                <img className="pacoteTodos" src="https://magazine.zarpo.com.br//wp-content/uploads/2015/08/san-andres-caribe-colombiano-zarpo-magazine.jpg" alt="img" />
                <div className="pacote-container"> >
                <p className="nomeTodos"> Lisboa em 3 dias</p>
                    <p className="valorTodos"> A partir de </p>
                    <p className="valorTodos1">R$ 2.900,00 por pessoa</p>
                </div>
                <div className="pacote-container">
                    <p className="nomeTodos">New York em 7 dias</p>
                    <p className="valorTodos"> A partir de </p>
                    <p className="valorTodos1">R$ 9.800,00 por pessoa</p>
                </div>
                <div className="pacote-container">
                    <p className="nomeTodos">San Andres em 5 dias</p>
                    <p className="valorTodos"> A partir de </p>
                    <p className="valorTodos1">R$ 4.500,00 por pessoa</p>
                </div>

                <img className="pacoteTodos" src="http://www.boaideiaviagens.com.br/wp-content/uploads/2017/03/porto-de-galinhas-pacote-de-viagem-1.jpg" alt="img" />
                <img className="pacoteTodos" src="https://casadoturista.com.br/wp-content/uploads/2014/11/dwn_grande_71-1024x530.jpg" alt="img" />
                <img className="pacoteTodos" src="https://dob5zu6vfhpfk.cloudfront.net/images/2016/06/31123853/Caldas-Novas-em-GO-600x288.jpg" alt="img" />
                <div className="pacote-container">
                    <p className="nomeTodos"> Porto de Galinhas em 7 dias</p>
                    <p className="valorTodos"> A partir de </p>
                    <p className="valorTodos1">R$ 4.500,00 por casal</p>
                </div>
                <div className="pacote-container">
                    <p className="nomeTodos">Beto Carrero em 5 dias</p>
                    <p className="valorTodos"> A partir de </p>
                    <p className="valorTodos1">R$ 2.200,00 por pessoa</p>
                </div>
                <div className="pacote-container">
                    <p className="nomeTodos">Caldas Novas em 5 dias</p>
                    <p className="valorTodos"> A partir de </p>
                    <p className="valorTodos1">R$ 900,00 por pessoa</p>
                </div>


                <img className="pacoteTodos" src="https://media-cdn.tripadvisor.com/media/photo-s/03/9b/2f/ec/natal.jpg" alt="img" />
                <img className="pacoteTodos" src="http://brasiliatour.com.br/wp-content/uploads/2012/02/beiramar.jpg" alt="img" />
                <img className="pacoteTodos" src="http://www.clickviagens.net/wp-content/uploads/2017/05/porto_de_galinhas_20.jpg" alt="img" />
                <div className="pacote-container">
                    <p className="nomeTodos">Natal em 7 dias</p>
                    <p className="valorTodos"> A partir de </p>
                    <p className="valorTodos1">R$ 2.500,00 por pessoa</p>
                </div>
                <div className="pacote-container">
                    <p className="nomeTodos">Fortaleza em 8 dias</p>
                    <p className="valorTodos"> A partir de </p>
                    <p className="valorTodos1">R$ 1.200,00 por pessoa</p>
                </div>
                <div className="pacote-container">
                    <p className="nomeTodos">Maceio em 7 dias</p>
                    <p className="valorTodos"> A partir de </p>
                    <p className="valorTodos1">R$ 2.700,00 por pessoa</p>
                </div>


            </div>

        </div>
    )
}

export default Vitrine;