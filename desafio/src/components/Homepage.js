import React from 'react'
import './homepage.css'
import CarouselHome from './carrossel';
import Vitrine from './Vitrine';
import { NavLink } from 'react-router-dom'


const Homepage = () => {
    return (
        <div>
            <img classeName="logoAzul" alt="img" src="https://www.azulviagens.com.br/handlers/ImageRequest.ashx?path=%2fimg%2flogo.png&mediaSize=lg&imgSize=original" />
            <div className="container-acesso">

                <text classeName="telefone"> 4002-8922</text>
                <NavLink to={"/login"}><button className="acessoLogin" ><span>Acesso</span></button></NavLink>

                <NavLink to={"/cadastro"}><button className="acessoCadastro"><span>Cadastro</span></button></NavLink>


            </div>
            <CarouselHome />

            <Vitrine />
        </div>


    )
}

export default Homepage